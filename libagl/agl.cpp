#define GLEW_STATIC
#include <GL/glew.c>

#include <agl.h>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <stdexcept>
#include <unordered_map>
#include <cmath>

#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

//#define cimg_use_jpeg
//#include <cimg/cimg.h>

//Images
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
//Font
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>

//Physics
/*#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/btBulletCollisionCommon.h>
*/

#include <Timer.cpp>
#include <Util.cpp>
#include <Input.cpp>
#include <Transform.cpp>
#include <Texture.cpp>
#include <Mesh.cpp>
#include <Material.cpp>
#include <Gameobject.cpp>
#include <Camera.cpp>
#include <GBuffer.cpp>
#include <Light.cpp>
#include <Ambientlight.cpp>
#include <Pointlight.cpp>
#include <Ui.cpp>
#include <Canvas.cpp>
#include <Chunk.cpp>
#include <Scene.cpp>
#include <Window.cpp>
#include <Application.cpp>
#include <Script.cpp>

template <class C> void safeDelete(C *thing){
	if(thing!=NULL)
		delete thing;
}

MeshContainer::MeshContainer(){
	mesh=NULL;
}
MeshContainer::~MeshContainer(){}

MaterialContainer::MaterialContainer(){
	material=NULL;
}
MaterialContainer::~MaterialContainer(){}

void glfwWindowResizeCallback(GLFWwindow *window, int width, int height){
	glViewport(0, 0, width, height);
	//screen=vec2(width, height);
	//currentGBuffer->resize(width, height);
}
static int init(){
	chdir("./Exec/");
}
static int init2D(){

	init();
		
	glfwSetErrorCallback(glfwErrorCallback);

	if(!glfwInit())
		errorlogquit("glfw could not be initialized", -1);
		
	stbi_set_flip_vertically_on_load(1);
	
}
static int init3D(){

	init2D();
}

static void cleanup(){
	safeDelete(Ambientlight::ambientlightProgram);
	safeDelete(Pointlight::mesh);
	safeDelete(Pointlight::pointlightProgram);
}
