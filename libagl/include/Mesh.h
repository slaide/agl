class DL_EXPORT Mesh{
	private:
		class MeshProcessor{
			public:
				friend class boost::serialization::access;
				template<class Archive>
				void serialize( Archive & ar, const unsigned int file_version);
				template<class Archive>
				void save(Archive & ar, const unsigned int version) const;
				template<class Archive>
				void load(Archive & ar, const unsigned int version);

				static Assimp::Importer importer;

				std::vector<uint32_t> faceData;
				std::vector<float> vertexPositions;
				std::vector<float> vertexNormals;
				std::vector<float> vertexCoordinates;

				bool hasNormals, isTextured;
				uint32_t numFaces, numVertices, vertexArray, vertexBuffer, indexBuffer, vertexNormalBuffer, vertexTexturecoordID;

				MeshProcessor(std::string file);
				MeshProcessor();
				~MeshProcessor();

				void sendToGpu();
		};
	
		static std::unordered_map<std::string, Mesh*> meshTable;
		static std::string directory, filetype;
		
		static Mesh loadArchivedMesh(std::string _archive);
		
		
		bool hasNormals, isTextured;
		uint32_t numFaces, numVertices, vertexArray, vertexBuffer, indexBuffer, vertexNormalBuffer, vertexTexturecoordID;

	public:
	
		static Mesh *getMesh(std::string _name);
		static void archiveMesh(std::string _meshstring, std::string _archive="");
		
		Mesh();
		Mesh(const Mesh & _mesh);
		~Mesh();
		
		void draw();
		
};
