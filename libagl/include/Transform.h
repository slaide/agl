//math interface for lua
class Vec3{
	public:
		glm::vec3 vec;
	
		Vec3(const glm::vec3 & v):vec(v){}
		
		Vec3(float _v):vec(_v){}
		Vec3(float _x, float _y, float _z):vec(_x, _y, _z){}
		Vec3(const Vec3 & v):vec(v.vec){}
		Vec3(Vec3 && v):vec(v.vec){}
		~Vec3(){}
		
		Vec3 operator+(const Vec3 & v) const{
			return Vec3(vec+v.vec);
		}
		Vec3 operator-(const Vec3 & v) const{
			return Vec3(vec-v.vec);
		}
		Vec3 operator*(float f){
			return Vec3(vec*f);
		}
		Vec3 operator/(float f){
			return Vec3(vec/f);
		}
		Vec3 rotateX(float angle){
			return Vec3(glm::rotateX(vec, glm::radians(angle))); 
		}
		Vec3 rotateY(float angle){
			return Vec3(glm::rotateY(vec, glm::radians(angle))); 
		}
		Vec3 rotateZ(float angle){
			return Vec3(glm::rotateZ(vec, glm::radians(angle))); 
		}
		float getX() const{return vec.x;}
		float getY() const{return vec.y;}
		float getZ() const{return vec.z;}
		void setX(float a){vec.x=a;}
		void setY(float a){vec.y=a;}
		void setZ(float a){vec.z=a;}
		
		float length() const{
			return vec.length();
		}
		void normalize(){
			float l=length();
			vec/=l;
		}
};
class Vec2{
	public:
		glm::vec2 vec;
		
		Vec2(const glm::vec2 & v):vec(v){}
		Vec2(const Vec2 & v):vec(v.vec){}
		Vec2(Vec2 && v):vec(v.vec){}
		~Vec2(){}
		
		Vec2 operator+(const Vec2 & v) const{
			return Vec2(vec+v.vec);
		}
		Vec2 operator-(const Vec2 & v) const{
			return Vec2(vec-v.vec);
		}
		Vec2 operator*(float f){
			return Vec2(vec*f);
		}
		Vec2 operator/(float f){
			return Vec2(vec/f);
		}
		
		float getX() const{return vec.x;}
		float getY() const{return vec.y;}
		void setX(float _x){vec.x=_x;}
		void setY(float _y){vec.y=_y;}
};

class DL_EXPORT Transform{
	private:
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
		
	public:
		
		vec3 position, rotation, scale;
	
		Transform(const vec3 _position=vec3(.0f), const vec3 _rotation=vec3(.0f), const vec3 _scale=vec3(1.f));
		Transform(const Vec3 _position=Vec3(.0f), const Vec3 _rotation=Vec3(.0f), const Vec3 _scale=Vec3(1.f));
		~Transform();
		
		Vec3 getPosition() const;
		Vec3 getRotation() const;
		Vec3 getScale() const;
		void setPosition(Vec3 _v);
		void setRotation(Vec3 _v);
		void setScale(Vec3 _v);
};
