template <class C>
class DL_EXPORT Script{
	private:	
		static std::string directory;
	
		lua_State *state;
		luabridge::LuaRef *funcs;
		
		void stdstuff(C* me);
	public:
		Script(std::string _file, C *me);
		~Script();
		
		int update();
		int fixedUpdate();
};
