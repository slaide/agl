class DL_EXPORT Material{
	public:
		class Attribute{
			public:
				//visitor lambda function only used for static type cast from variant
				static Attribute *shit;
				
				const char* name;
				int32_t location;
				
				Attribute(const char *name, GLint program);
				~Attribute();
				
				inline void submit(int32_t value);
				inline void submit(uint32_t value);
				inline void submit(float value);
				inline void submit(float *value);
				inline void submit(vec2 value);
				inline void submit(vec3 value);
				inline void submit(vec4 value);
				inline void submit(mat4 value);
		};
		
		std::vector<Attribute> attributes;
		uint32_t program;
		vec3 diffusecolor, specularcomponents;
		
		Material();
		Material(const char *vertexShader, const char *fragmentShader);
		~Material();
		
		int addAttribute(const char *name);
};
