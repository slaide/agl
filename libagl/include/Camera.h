class DL_EXPORT Camera:public Gameobject{

	private:
	
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
		
		vec3 target;
		mat4 projection, view;
		
	public:
	
		float fovx, fovy, aspectRatio, nearClip, farClip;
		
		Camera();
		Camera(float _fovy, float _aspectRatio, float _nearClip, float _farClip);
		~Camera();
		
		//void calcModelMatrix();
		void setInitial();
		vec3 getTarget() const;
		mat4 getView();
		mat4 getProjection() const;
		Vec3 getTargetLua() const;
		void setScript(const std::string & file);
		
		void draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds);
};
