class DL_EXPORT Ui{
	private:
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
		
		static uint64_t nextid;
		
		uint64_t id;
		bool enabled;
		
	protected:
	
		void draw();
		
	public:
	
		static Mesh *mesh;
		static Material *material;
		static uivec2wrapper *slider;
		
		vec2 position, size;
		vec4 color;
		std::vector<Ui> children;
		Texture *texture;
		bool mytexture;
		Keystate mousestate;
		
		GLfloat value;//for sliders
		
		Ui();
		Ui(std::string file, vec2 position, vec2 size, vec2 dadspos);
		Ui(vec4 color, vec2 position, vec2 size, vec2 dadspos);
		~Ui();
		
		void enable();
		void disable();
		bool isEnabled() const;
		void setEnabled(bool _enabled);
		void setId();
		uint64_t getId() const;
		Ui* getLastChild();
		Ui* addChild(vec4 color=vec4(1.f), vec2 pos=vec2(.0f), vec2 size=vec2(300.f), vec2 dadspos=vec2(0.f));
		Ui* addChild(std::string & _texture, vec2 pos=vec2(.0f), vec2 size=vec2(300.f), vec2 dadspos=vec2(0.f));
		Ui* addChild(const char *_texture, vec2 pos=vec2(.0f), vec2 size=vec2(300.f), vec2 dadspos=vec2(0.f));
		Ui *addHorizontalSlider(vec4 color=vec4(1.f), vec2 pos=vec2(.0f), vec2 size=vec2(300.f), vec2 dadspos=vec2(0.f));
		float getHorizontalSliderValue();
};
