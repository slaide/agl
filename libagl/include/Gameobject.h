class DL_EXPORT Gameobject{

	private:
	
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
				
		static uint64_t nextid;
		
	protected:
		
		unsigned char type;
		std::string name;
		Transform transform;
		bool enabled;
		uint64_t id;
		MeshContainer mc;
		MaterialContainer maco;
		bool drawable;
		mat4 mvp, modelMatrix;
		std::vector<Gameobject*> children;
		Script<Gameobject> *script;
		
	public:

		static Material *defaultMaterial;
		
		Gameobject();
		Gameobject(std::string _name);
		Gameobject(const Gameobject & _gameobject);
		~Gameobject();
		
		void calcModelMatrix();
		void setId();
		uint64_t getId() const;
		virtual void draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds);
		virtual void draw(Camera * _camera);
		bool isEnabled() const;
		void setEnabled(bool _enabled);
		void enableDrawing();
		void disbleDrawing();
		virtual void setScript(const std::string & file);
		Script<Gameobject> *getScript() const;
		void setMesh(std::string _mesh);
		std::string getName() const;
		void setName(const std::string & _name);
		Transform getTransform() const;
		vec3 getPosition() const;
		vec3 getRotation() const;
		vec3 getScale() const;
		void setTransform(const Transform & _transform);
		void setPosition(const vec3 & _position);
		void setRotation(const vec3 & _rotation);
		void setScale(const vec3 & _scale);
		void addCamera(float fov=45.f, float aspectRatio=16.f/9.f, float nearClip=0.1f, float farClip=1000.f);
		void addGameobject(std::string _name);
		void addAmbientlight(vec4 color);
		void addPointlight(vec3 color, vec3 position, vec3 attenuation);
		bool removeChild(uint64_t id);
		bool removeChild(const std::string & _name);
		Gameobject *find(uint64_t id);
		Gameobject *find(const std::string & _name);
		Gameobject *getLastChild();
		Gameobject *spawn(const Transform & _transform);
};
