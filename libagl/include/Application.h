class DL_EXPORT Application{

	protected:
	
		std::string title;
		
	public:
	
		Application(const char *name);
		~Application();
		
		virtual void run();
		virtual void update()=0;
		
		std::string getName();
};

class DL_EXPORT Application2D: public Application{

	protected:
	
		std::vector<Window2D> windows;
		
	public:
	
		Application2D(const char *name, std::vector<int[4]> windowInfo);
		Application2D(const char *name, int windowInfo[4]);
		~Application2D();
		
		void closeWindow(int pos);
		void run();
		virtual void update();
		Window2D *getWindow(uint32_t a);
		Window2D *getWindow(GLFWwindow *_window);
		void processInput();
};

class DL_EXPORT Application3D: public Application{

	protected:
	
		std::vector<Window3D> windows;
		
	public:
	
		Application3D(const char *name, int windowInfo[4]);
		~Application3D();
		
		void closeWindow(int pos);
		void run();
		virtual void update();
		void loadScene(std::string _file, uint32_t _windowIndex=0);
		Window3D *getWindow(uint32_t _a);
		Window3D *getWindow(GLFWwindow *_window);
		void close();
};
