class DL_EXPORT Light:public Gameobject{

	private:
	
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
		
	public:
	
		vec3 color;
		
		Light(vec3 & _color);
		Light(vec4 _color);
		Light();
		~Light();
		
		virtual void draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds)=0;
};
