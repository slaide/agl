GLboolean DL_EXPORT comparegreater(vec2 a, vec2 b);
GLboolean DL_EXPORT comparesmaller(vec2 a, vec2 b);
int DL_EXPORT isin(vec2 point, vec2 lowerleft, vec2 upperright);
