class DL_EXPORT Timer{
	private:
		int years, months, days, hours, minutes, seconds;
	public:
		static Timer getSystemTime();
		
		Timer();
		Timer(struct tm *time);
		Timer(time_t time);
		~Timer();
		
		void print(FILE *file=stdout);
};
