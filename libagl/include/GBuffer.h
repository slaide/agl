//diffusecolor, normal, worldpos, specular, depth(is renderbuffer, but has own place in buffer id array), object pointer
#define GBUFFER_LAYERS 6
class DL_EXPORT GBuffer{
	private:
		vec2 size;
	public:
		GLuint numTextures, textures[GBUFFER_LAYERS], customFramebuffer;
		GLenum drawBuffers[GBUFFER_LAYERS-1];
		GBuffer(vec2 _size, uint32_t numTextures=GBUFFER_LAYERS);
		~GBuffer();
		void resize(uint32_t width, uint32_t height);
		void prepareforGPass();
		void prepareforLightPass();
		Gameobject *getObject(ivec2 pos);
};
