class DL_EXPORT Scene{

	private:
	
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
		
		static std::string directory, filetype;
		std::string name;
		std::vector<Ambientlight*> ambientlights;
		std::vector<Pointlight*> pointlights;
		std::vector<Gameobject*> scripteds;
		
		void drawAmbientlights(vec2 _screensize);
		void drawPointlights(vec2 _screensize);
		void drawCanvas(vec2 _screensize);
		
	public:
	
		std::vector<Chunk*> chunks;
		Canvas canvas;
		Camera *mainCamera;
		
		Scene();
		Scene(std::string & _name, vec2 _screenSize);
		~Scene();
		
		std::string getName();
		void setName(std::string _name);
		void bake(std::string archive="");
		static Scene load(std::string archive);
		
		void drawGeometry();
		void drawLightsAndUi(vec2 _screensize);
		
		bool removeGameobject(uint64_t id);
		bool removeGameobject(const std::string & _name);
		Gameobject *find(uint64_t id);
		Gameobject *find(const std::string & _name);
		
		void loadChunk(std::string _archive);
		void loadChunk(int num);
		void saveChunks();
};
