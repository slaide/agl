class DL_EXPORT Pointlight: public Light{

	private:
	
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
		
	public:
	
		static Material *pointlightProgram;
		static Mesh *mesh;
		
		static void prepareDraw(vec2 _screensize, Camera *_camera);
		
		GLfloat constant, linear, exponent, diffuse, specularPower, specular;
		
		Pointlight(GLfloat _diffuse, vec3 & _color, vec3 & _attenuation, vec3 _newposition=vec3(0.f));
		Pointlight();
		~Pointlight();
		
		//void calcModelMatrix()
		void calcScale();
		void draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds);
		void draw(Camera * _camera);
};	
