void DL_EXPORT log(std::string msg);

void DL_EXPORT log(const char *msg);

void DL_EXPORT errorlog(std::string msg);

void DL_EXPORT errorlogquit(std::string msg, int error);

void DL_EXPORT errorlogquit(const char *msg, int error);

void DL_EXPORT glfwErrorCallback(int error, const char *errormsg);

void DL_EXPORT glfwWindowResizeCallback(GLFWwindow *window, int width, int height);

//check how this can be done as template for more customization
class DL_EXPORT uivec2wrapper{
	void(* fptr)(void*, vec2);
	public: 
		uivec2wrapper(void(*fct)(void*, vec2));
		void operator()(void* me, vec2 pos);
};
