class DL_EXPORT Ambientlight: public Light{

	private:
	
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
		
	public:
	
		static Material *ambientlightProgram;
		static Mesh *mesh;
		
		static void prepareDraw(vec2 _screensize);
		
		float ambient;
		
		Ambientlight();
		Ambientlight(vec4 & color);
		~Ambientlight();
		
		//void calcModelMatrix();
		void draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds);
		void draw(Camera * _camera);
};
