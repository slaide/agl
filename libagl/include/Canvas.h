class DL_EXPORT Canvas:public Ui{
	private:
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int file_version);
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const;
		template<class Archive>
		void load(Archive & ar, const unsigned int version);
	public:
		static vec2 fhd;
		
		float referenceScale;
		
		Canvas(vec4 color, vec2 size, vec2 position=vec2(0.f));
		Canvas();
		~Canvas();
		
		void drawAll(vec2 _screensize);
};
