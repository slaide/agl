#if defined(_DLL)
	#define DL_EXPORT __declspec(dllexport)
#elif defined(STATIC)
	#define DL_EXPORT
#else
	#define DL_EXPORT __declspec(dllimport)
#endif

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include <string>
#include <vector>
#include <chrono>
#include <unordered_map>
#include <type_traits>

#include <unistd.h>
#include <time.h>
#include <stdlib.h>

constexpr float pi = 3.1415926f;

#include <GLFW/glfw3.h>

#include <assimp/Importer.hpp>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
typedef glm::vec2 vec2;
typedef glm::ivec2 ivec2;
typedef glm::vec3 vec3;
typedef glm::vec4 vec4;
typedef glm::highp_mat4 mat4;

#include <boost/serialization/serialization.hpp>

#include <lua.hpp>
#include <LuaBridge/LuaBridge.h>

//namespace gle {

enum Mousestate {disabled, enabled};
enum Mousebutton {left, right, middle};
enum Key {A=65, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, NUM0=48, NUM1, NUM2, NUM3, NUM4, NUM5, NUM6, NUM7, NUM8, NUM9, SPACE=32, TAB=11, ENTER=13, ESC=27};
enum Keystate {unpressed, down, pressed, up};
//static vec2 screen(1280, 720);
static vec4 v1(1.f);
static GLint gbufferTextures[3]={0,1,2};
static vec3 globRight(1.f, .0f, .0f) , globUp(0.f, 1.f, .0f), globIn(.0f, .0f, -1.f);

template <class C> void DL_EXPORT safeDelete(C *thing);

class Gameobject;
class Camera;
class Ambientlight;
class Pointlight;
template <class>
class Script;
class Luascript;

class Application3D;
extern "C" { Application3D * DL_EXPORT app;}

#include "Timer.h"
#include "Util.h"
#include "Input.h"
#include "Transform.h"
#include "Texture.h"
#include "Mesh.h"
#include "Material.h"
class DL_EXPORT MeshContainer{
	public:
		std::string name;
		Mesh *mesh;
		MeshContainer();
		~MeshContainer();
};
class DL_EXPORT MaterialContainer{
	public:
		std::string name;
		Material *material;
		MaterialContainer();
		~MaterialContainer();
};
#include "Gameobject.h"
#include "Camera.h"
#include "GBuffer.h"
#include "Light.h"
#include "Ambientlight.h"
#include "Pointlight.h"
#include "Ui.h"
#include "Canvas.h"
#include "Chunk.h"
#include "Scene.h"
#include "Window.h"

static int init();
static int init2D();
static int init3D();

static void cleanup();

#include "Application.h"
#include "Script.h"

//}
