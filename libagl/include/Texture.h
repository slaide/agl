class DL_EXPORT Texture{
	private:
		class ftstuff{
			public:
				ftstuff();
		};
		static ftstuff dunno;
	public:
		GLuint texture;
		vec2 size;
		Texture(std::string & _image);
		~Texture();
};
