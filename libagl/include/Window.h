class DL_EXPORT Window2D{

	public:
	
		GLFWwindow* window;
		ivec2 position;
		
		//utf-32:0xba should cover enough keys
		Keystate keys[0xba], simplekeys[40+3];
		bool keyupdates[0xba], simplekeyupdates[40+3];
		ivec2 absoluteMousePosition, relativeMousePosition, prevmpos;
		bool mouseEnabled;
		
	//public:
	
		ivec2 size;
		Canvas canvas;
		Window2D(std::string name, int descriptor[4]);
		~Window2D();
		
		void draw();
		void close();
		bool closed();
		void makeActive();
		Canvas *getCanvas();
		void updateInput();
		
		static void glfwMousePositionCallback(GLFWwindow *_window, double absolutexPosition, double absoluteyPosition);
		static void glfwMouseButtonCallback(GLFWwindow *_window, int button, int action, int mods);
		static void glfwCharCallback(GLFWwindow *_window, uint32_t unicodecharacter);
		static void glfwKeyCallback(GLFWwindow *_window, int key, int scancode, int action, int mods);
		
		void setMouse(Mousestate able);
		void setMouse(bool able);
		Keystate getMousebuttonstate(Mousebutton button);
		Keystate getkeystate(uint32_t key);
		Keystate getsimplekeystate(char keycode);
		Keystate getsimplekeystate(Key keycode);
};

class DL_EXPORT Window3D: public Window2D{

	private:
	
		static std::string start;
		Scene baseScene;
		GBuffer gBuffer;
		
	public:
	
		Window3D(std::string title, int descriptor[4]);
		~Window3D();
		
		void loadScene(std::string archive);
		void draw();
		Scene *getBaseScene();
		Canvas *getCanvas();
		
		Gameobject *getObject(ivec2 pos);
};
