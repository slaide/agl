Timer Timer::getSystemTime(){
	std::chrono::time_point<std::chrono::system_clock> sysclock=std::chrono::system_clock::now();
	Timer systimestruct(std::chrono::system_clock::to_time_t(sysclock));
	return systimestruct;
}

Timer::Timer(){}
Timer::Timer(struct tm *time){
	this->seconds=time->tm_sec;
	this->minutes=time->tm_min;
	this->hours=time->tm_hour;
	this->days=time->tm_mday;
	this->months=time->tm_mon+1;
	this->years=time->tm_year+1900;
}
Timer::Timer(time_t time){
	struct tm *timestruct=localtime(&time);
	this->seconds=timestruct->tm_sec;
	this->minutes=timestruct->tm_min;
	this->hours=timestruct->tm_hour;
	this->days=timestruct->tm_mday;
	this->months=timestruct->tm_mon+1;
	this->years=timestruct->tm_year+1900;
}
Timer::~Timer(){}

void Timer::print(FILE *file){
	fprintf(file, "%04i-%02i-%02i %02i:%02i:%02i", this->years, this->months, this->days, this->hours, this->minutes, this->seconds);
}
