Texture::ftstuff::ftstuff(){
	;
}
Texture::Texture(std::string & _image){
	int x,y,n;
	unsigned char *image=stbi_load(_image.c_str(), &x, &y, &n, 0);
	
	if(!image)
		errorlogquit("unable to load image", -8);
		
	size=ivec2(x, y);
		
	glGenTextures(1, &this->texture);
	glBindTexture(GL_TEXTURE_2D, this->texture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, n);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	switch(n){
		case 3: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size.x, size.y, 0, GL_RGB, GL_UNSIGNED_BYTE, image); break;
		case 4: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, image); break;
	}
	
	stbi_image_free(image);
}
Texture::~Texture(){
	glDeleteTextures(1, &this->texture);
}
