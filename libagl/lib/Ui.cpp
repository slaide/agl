template<class Archive>
void Ui::serialize(Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Ui::save(Archive & ar, const unsigned int version) const
{
	ar << position.x << position.y;
	ar << size.x << size.y;
	ar << color.x << color.y << color.z << color.w;
	ar << children;
	ar << enabled;
}
template<class Archive>
void Ui::load(Archive & ar, const unsigned int version)
{
	ar >> position.x >> position.y;
	ar >> size.x >> size.y;
	ar >> color.x >> color.y >> color.z >> color.w;
	ar >> children;
	ar >> enabled;
	getId();
}
void Ui::draw(){
	if(this->texture!=NULL){
		glActiveTexture(GL_TEXTURE0+GBUFFER_LAYERS);
		glBindTexture(GL_TEXTURE_2D, this->texture->texture);
	}
		
	this->material->attributes[0].submit(GBUFFER_LAYERS);
	this->material->attributes[1].submit(this->size);
	this->material->attributes[2].submit(this->position);
	this->material->attributes[3].submit(this->color);
	this->mesh->draw();
	for(auto & el: children)
		el.draw();
}

Ui::Ui(){}
Ui::Ui(std::string file, vec2 position, vec2 size, vec2 dadspos): texture(new Texture(file)), mousestate(unpressed){
	this->color.x=-1.f;
	this->mytexture=true;
	this->enabled=true;
	this->position=position+dadspos;
	this->size=size;
	this->id=nextid++;
}
Ui::Ui(vec4 color, vec2 position, vec2 size, vec2 dadspos): texture(NULL), mousestate(unpressed){
	this->color=color;
	this->enabled=true;
	this->position=position+dadspos;
	this->size=size;
	this->id=nextid++;
}
Ui::~Ui(){
	if(this->mytexture&&(this->color.x<0.f))
		delete this->texture;
}

void Ui::enable(){
	enabled=true;
}
void Ui::disable(){
	enabled=false;
}
bool Ui::isEnabled() const{
	return enabled;
}
void Ui::setEnabled(bool _enabled){
	enabled=_enabled;
}
void Ui::setId(){
	id=nextid++;
}
uint64_t Ui::getId() const{
	return id;
}
Ui* Ui::getLastChild(){
	return &this->children.back();
}
Ui *Ui::addChild(vec4 color, vec2 pos, vec2 size, vec2 dadspos){
	this->children.emplace_back(color, pos, size, dadspos);
	return this->getLastChild();
}
Ui *Ui::addChild(std::string & _texture, vec2 pos, vec2 size, vec2 dadspos){
	this->children.emplace_back(_texture, pos, size, dadspos);
	return this->getLastChild();
}
Ui *Ui::addChild(const char *_texture, vec2 pos, vec2 size, vec2 dadspos){
	this->children.emplace_back(_texture, pos, size, dadspos);
	return this->getLastChild();
}
Ui *Ui::addHorizontalSlider(vec4 color, vec2 pos, vec2 size, vec2 dadspos){
	Ui *a=this->addChild(color, pos, size, dadspos);
	a->addChild(color*0.5f, vec2(0.f), vec2(a->size.y), a->position);
	//a->mouseOver=slider;
}
float Ui::getHorizontalSliderValue(){
	if (this->children.empty())
		return 0x00000000;
		
	float length=this->size.x-this->children.at(0).size.x;
	float result=(this->children.at(0).position.x-this->position.x)/length;
	
	return result;
}

Mesh *Ui::mesh;
Material *Ui::material;
uint64_t Ui::nextid=0;
