Window2D::Window2D(std::string name, int descriptor[4]): mouseEnabled(true){
	size.x=descriptor[0];
	size.y=descriptor[1];
	position.x=descriptor[2];
	position.y=descriptor[3];
	this->canvas=Canvas(vec4(1.f, 1.f, 1.f, 0.f), vec2(size.x, size.y), vec2(0.f));
	
	int monitorcount;
	auto monitors=glfwGetMonitors(&monitorcount);
	auto mode = glfwGetVideoMode((monitorcount==1)?monitors[0]:monitors[1]);
	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	glfwWindowHint(GLFW_SAMPLES, 1);
	glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_SRGB_CAPABLE, GLFW_TRUE);
	glfwWindowHint(GLFW_CONTEXT_RELEASE_BEHAVIOR, GLFW_RELEASE_BEHAVIOR_FLUSH);
	
	window=glfwCreateWindow(size.x, size.y, name.c_str(), NULL, NULL);
	if(!window)
		errorlogquit("failed to create window", -8);
	
	makeActive();
	
	glewExperimental=GL_TRUE;
	if(glewInit()!=GLEW_OK)
		errorlogquit("could not initialize glew", -3);
	
	glfwSwapInterval(1);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, GL_TRUE);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetWindowSizeCallback(window, glfwWindowResizeCallback);
	glfwSetCursorPosCallback(window, glfwMousePositionCallback);
	glfwSetCharCallback(window, glfwCharCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);
	glfwSetMouseButtonCallback(window, glfwMouseButtonCallback);

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	//screen=size;
	//assert(width==size.x);
	//assert(height=size.y);
	glViewport(0, 0, width, height);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_TEXTURE_2D);
	glFrontFace(GL_CCW);

	//needs to be per-window
	GLdouble eins, zwei;
	glfwGetCursorPos(window, &eins, &zwei);
	absoluteMousePosition.x=prevmpos.x=(GLint)eins;
	absoluteMousePosition.y=prevmpos.y=(GLint)zwei;
	glfwSetWindowPos(window, position.x, position.y);

	Ui::material=new Material("./shaders/flatsurface.vs", "./shaders/flatsurface.fs");
	Ui::material->addAttribute("image");
	Ui::material->addAttribute("size");
	Ui::material->addAttribute("relposition");
	Ui::material->addAttribute("bgcolor");
	Ui::material->addAttribute("canvassize");
	Ui::material->addAttribute("canvasposition");
	Ui::material->addAttribute("screen");

	Ui::mesh=Mesh::getMesh("screen");
	
	setMouse(enabled);
}
Window2D::~Window2D(){
	glfwDestroyWindow(window);
}

void Window2D::draw(){
	makeActive();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	canvas.drawAll(size);
	glfwSwapBuffers(window);
}
void Window2D::close(){
	glfwSetWindowShouldClose(window, GLFW_TRUE);
}
bool Window2D::closed(){
	return glfwWindowShouldClose(window);
}
void Window2D::makeActive(){
	glfwMakeContextCurrent(window);
}
Canvas *Window2D::getCanvas(){
	return &canvas;
}
void Window2D::updateInput(){
	for(unsigned int a=0;a<43;a++){
		if(simplekeyupdates[a])
			switch(simplekeys[a]){
				case 0: simplekeys[a]=down; break;
				case 1:// simplekeys[a]=pressed; break;
				case 2: simplekeys[a]=pressed; break;
				case 3: simplekeys[a]=down; break;
			}
		else
			switch(simplekeys[a]){
				case 0: simplekeys[a]=unpressed; break;
				case 1:// simplekeys[a]=down; break;
				case 2: simplekeys[a]=up; break;
				case 3: simplekeys[a]=unpressed; break;
			}
	}
	
	relativeMousePosition.x=absoluteMousePosition.x-prevmpos.x;
	relativeMousePosition.y=prevmpos.y-absoluteMousePosition.y;
	prevmpos=absoluteMousePosition;
}

void Window2D::glfwMousePositionCallback(GLFWwindow *window, double absolutexPosition, double absoluteyPosition){
	auto win=app->getWindow(window);
	win->absoluteMousePosition.x=absolutexPosition;
	win->absoluteMousePosition.y=win->size.y-absoluteyPosition;
}
void Window2D::glfwMouseButtonCallback(GLFWwindow *window, int button, int action, int mods){
	if(button>2)
		return;
	auto win=app->getWindow(window);
	win->simplekeyupdates[40+button]=action;
}
//utf-32
void Window2D::glfwCharCallback(GLFWwindow *window, uint32_t unicodecharacter){
	auto win=app->getWindow(window);
	win->keyupdates[unicodecharacter%0xba]=true;
}
void Window2D::glfwKeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods){
	bool *simplekeyupdates=app->getWindow(window)->simplekeyupdates;
	
	switch(key){
		case GLFW_KEY_A:simplekeyupdates[0]=action; break;
		case GLFW_KEY_B:simplekeyupdates[1]=action; break;
		case GLFW_KEY_C:simplekeyupdates[2]=action; break;
		case GLFW_KEY_D:simplekeyupdates[3]=action; break;
		case GLFW_KEY_E:simplekeyupdates[4]=action; break;
		case GLFW_KEY_F:simplekeyupdates[5]=action; break;
		case GLFW_KEY_G:simplekeyupdates[6]=action; break;
		case GLFW_KEY_H:simplekeyupdates[7]=action; break;
		case GLFW_KEY_I:simplekeyupdates[8]=action; break;
		case GLFW_KEY_J:simplekeyupdates[9]=action; break;
		case GLFW_KEY_K:simplekeyupdates[10]=action; break;
		case GLFW_KEY_L:simplekeyupdates[11]=action; break;
		case GLFW_KEY_M:simplekeyupdates[12]=action; break;
		case GLFW_KEY_N:simplekeyupdates[13]=action; break;
		case GLFW_KEY_O:simplekeyupdates[14]=action; break;
		case GLFW_KEY_P:simplekeyupdates[15]=action; break;
		case GLFW_KEY_Q:simplekeyupdates[16]=action; break;
		case GLFW_KEY_R:simplekeyupdates[17]=action; break;
		case GLFW_KEY_S:simplekeyupdates[18]=action; break;
		case GLFW_KEY_T:simplekeyupdates[19]=action; break;
		case GLFW_KEY_U:simplekeyupdates[20]=action; break;
		case GLFW_KEY_V:simplekeyupdates[21]=action; break;
		case GLFW_KEY_W:simplekeyupdates[22]=action; break;
		case GLFW_KEY_X:simplekeyupdates[23]=action; break;
		case GLFW_KEY_Y:simplekeyupdates[24]=action; break;
		case GLFW_KEY_Z:simplekeyupdates[25]=action; break;
		case GLFW_KEY_0:simplekeyupdates[26]=action; break;
		case GLFW_KEY_1:simplekeyupdates[27]=action; break;
		case GLFW_KEY_2:simplekeyupdates[28]=action; break;
		case GLFW_KEY_3:simplekeyupdates[29]=action; break;
		case GLFW_KEY_4:simplekeyupdates[30]=action; break;
		case GLFW_KEY_5:simplekeyupdates[31]=action; break;
		case GLFW_KEY_6:simplekeyupdates[32]=action; break;
		case GLFW_KEY_7:simplekeyupdates[33]=action; break;
		case GLFW_KEY_8:simplekeyupdates[34]=action; break;
		case GLFW_KEY_9:simplekeyupdates[35]=action; break;
		case GLFW_KEY_SPACE:simplekeyupdates[36]=action; break;
		case GLFW_KEY_TAB:simplekeyupdates[37]=action; break;
		case GLFW_KEY_ENTER:simplekeyupdates[38]=action; break;
		case GLFW_KEY_ESCAPE:simplekeyupdates[39]=action; break;
	}
}

void Window2D::setMouse(Mousestate able){
	glfwSetInputMode(window, GLFW_CURSOR, ((int)able)?GLFW_CURSOR_NORMAL:GLFW_CURSOR_DISABLED);
	mouseEnabled=(bool)able;
}
void Window2D::setMouse(bool able){
	setMouse(able?enabled:disabled);
}
Keystate Window2D::getMousebuttonstate(Mousebutton button){
	return simplekeys[40+(int)button];
}
Keystate Window2D::getkeystate(uint32_t key){
	return unpressed;
}
Keystate Window2D::getsimplekeystate(char keycode){
	Keystate *simplekeys=app->getWindow(window)->simplekeys;
	
	switch(keycode){
		case 'A':return simplekeys[0]; break;
		case 'B':return simplekeys[1]; break;
		case 'C':return simplekeys[2]; break;
		case 'D':return simplekeys[3]; break;
		case 'E':return simplekeys[4]; break;
		case 'F':return simplekeys[5]; break;
		case 'G':return simplekeys[6]; break;
		case 'H':return simplekeys[7]; break;
		case 'I':return simplekeys[8]; break;
		case 'J':return simplekeys[9]; break;
		case 'K':return simplekeys[10]; break;
		case 'L':return simplekeys[11]; break;
		case 'M':return simplekeys[12]; break;
		case 'N':return simplekeys[13]; break;
		case 'O':return simplekeys[14]; break;
		case 'P':return simplekeys[15]; break;
		case 'Q':return simplekeys[16]; break;
		case 'R':return simplekeys[17]; break;
		case 'S':return simplekeys[18]; break;
		case 'T':return simplekeys[19]; break;
		case 'U':return simplekeys[20]; break;
		case 'V':return simplekeys[21]; break;
		case 'W':return simplekeys[22]; break;
		case 'X':return simplekeys[23]; break;
		case 'Y':return simplekeys[24]; break;
		case 'Z':return simplekeys[25]; break;
		case '0':return simplekeys[26]; break;
		case '1':return simplekeys[27]; break;
		case '2':return simplekeys[28]; break;
		case '3':return simplekeys[29]; break;
		case '4':return simplekeys[30]; break;
		case '5':return simplekeys[31]; break;
		case '6':return simplekeys[32]; break;
		case '7':return simplekeys[33]; break;
		case '8':return simplekeys[34]; break;
		case '9':return simplekeys[35]; break;
		case 32:return simplekeys[36]; break;
		case 11:return simplekeys[37]; break;
		case 13:return simplekeys[38]; break;
		case 27:return simplekeys[39]; break;
	}
}
Keystate Window2D::getsimplekeystate(Key keycode){
	return getsimplekeystate((char)keycode);
}

Window3D::Window3D(std::string title, int descriptor[4]):Window2D(title, descriptor), gBuffer(vec2(descriptor[0], descriptor[1])), baseScene(start, vec2(descriptor[0], descriptor[1])){
	Gameobject::defaultMaterial=new Material("./shaders/geom_pass.vs", "./shaders/geom_pass.fs");
	Gameobject::defaultMaterial->addAttribute("MVP");
	Gameobject::defaultMaterial->addAttribute("modelMatrix");

	Ambientlight::mesh=Mesh::getMesh("screen");
	Ambientlight::ambientlightProgram=new Material("./shaders/ambientlight.vs", "./shaders/ambientlight.fs");
	Ambientlight::ambientlightProgram->addAttribute("colorMap");
	Ambientlight::ambientlightProgram->addAttribute("normalMap");
	Ambientlight::ambientlightProgram->addAttribute("positionMap");
	Ambientlight::ambientlightProgram->addAttribute("lightColor");
	Ambientlight::ambientlightProgram->addAttribute("ambient");
	Ambientlight::ambientlightProgram->addAttribute("screenSize");

	Pointlight::mesh=Mesh::getMesh("sphere");
	Pointlight::pointlightProgram=new Material("./shaders/pointlight.vs", "./shaders/pointlight.fs");
	Pointlight::pointlightProgram->addAttribute("colorMap");
	Pointlight::pointlightProgram->addAttribute("normalMap");
	Pointlight::pointlightProgram->addAttribute("positionMap");

	Pointlight::pointlightProgram->addAttribute("MVP");
	Pointlight::pointlightProgram->addAttribute("modelMatrix");

	Pointlight::pointlightProgram->addAttribute("screenSize");
	Pointlight::pointlightProgram->addAttribute("light.diffuse");
	Pointlight::pointlightProgram->addAttribute("light.position");
	Pointlight::pointlightProgram->addAttribute("light.constant");
	Pointlight::pointlightProgram->addAttribute("light.linear");
	Pointlight::pointlightProgram->addAttribute("light.quadratic");
	Pointlight::pointlightProgram->addAttribute("light.specular");
	Pointlight::pointlightProgram->addAttribute("viewPos");
}
Window3D::~Window3D(){}

void Window3D::loadScene(std::string archive){
	baseScene=Scene::load(archive);
}
void Window3D::draw(){
	makeActive();
	gBuffer.prepareforGPass();
	baseScene.drawGeometry();
	
	gBuffer.prepareforLightPass();
	baseScene.drawLightsAndUi(vec2(size.x, size.y));
	
	glfwSwapBuffers(window);
}
Scene *Window3D::getBaseScene(){
	return &baseScene;
}
Canvas *Window3D::getCanvas(){
	return &baseScene.canvas;
}

Gameobject *Window3D::getObject(ivec2 pos){
	return gBuffer.getObject(pos);
}

std::string Window3D::start("start");
