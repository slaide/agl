template<class Archive>
void Chunk::serialize( Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Chunk::save(Archive & ar, const unsigned int version) const
{
	ar << boost::serialization::base_object<const Gameobject>(*this);
}
template<class Archive>
void Chunk::load(Archive & ar, const unsigned int version)
{
	ar >> boost::serialization::base_object<Gameobject>(*this);
}

Chunk::Chunk(){}
Chunk::~Chunk(){}

void Chunk::draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds){
	if(!isEnabled())
		return;
		
	switch(type){
		//case 0: break;
		case 1: {
				Ambientlight *al=(Ambientlight *)this; 
				ambientlights->push_back(al);
			} 
			break;
		case 2: {
				Pointlight *pl=(Pointlight *)this; 
				pointlights->push_back(pl); 
			}
			break;
	}
	
	if(script!=NULL)
		scripteds->push_back(this);
		
	for(auto _child:children)
		_child->draw(_camera, ambientlights, pointlights, scripteds);
}
