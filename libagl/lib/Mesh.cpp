template<class Archive>
void Mesh::MeshProcessor::serialize(Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Mesh::MeshProcessor::save(Archive & ar, const unsigned int version) const
{
	ar << faceData;
	ar << vertexPositions;
	ar << vertexNormals;
	ar << vertexCoordinates;
	ar << hasNormals;
	ar << isTextured;
	ar << numFaces;
	ar << numVertices;
}
template<class Archive>
void Mesh::MeshProcessor::load(Archive & ar, const unsigned int version)
{
	ar >> faceData;
	ar >> vertexPositions;
	ar >> vertexNormals;
	ar >> vertexCoordinates;
	ar >> hasNormals;
	ar >> isTextured;
	ar >> numFaces;
	ar >> numVertices;
	this->sendToGpu();
}
Mesh::MeshProcessor::MeshProcessor(std::string file){
	const aiScene *scene=importer.ReadFile(file, aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals | aiProcess_Triangulate);
	if(!scene){
		errorlog(std::string("failed to load") + std::string(file));
		exit;
	}
	aiMesh *aimesh=scene->mMeshes[0];
	if(!aimesh){
		errorlog("could not find a mesh");
		exit;
	}

	numFaces=aimesh->mNumFaces;
	numVertices=aimesh->mNumVertices;
	hasNormals=aimesh->HasNormals();
	isTextured=aimesh->HasTextureCoords(0);

	for(uint32_t i=0; i<numFaces; i++){
		auto faceinds=aimesh->mFaces[i].mIndices;
		faceData.push_back(faceinds[0]);
		faceData.push_back(faceinds[1]);
		faceData.push_back(faceinds[2]);
	}

	for(uint32_t i=0;i<numVertices; i++){
		auto vertex=aimesh->mVertices[i];
		vertexPositions.push_back(vertex[0]);
		vertexPositions.push_back(vertex[1]);
		vertexPositions.push_back(vertex[2]);
	}

	if(hasNormals)
		for(uint32_t i=0;i<numVertices; i++){
			auto vertex=aimesh->mNormals[i];
			vertexNormals.push_back(vertex[0]);
			vertexNormals.push_back(vertex[1]);
			vertexNormals.push_back(vertex[2]);
		}

	if(isTextured)
		for(uint32_t i=0;i<numVertices; i++){
			auto vertex=aimesh->mTextureCoords[0][i];
			vertexCoordinates.push_back(vertex[0]);
			vertexCoordinates.push_back(vertex[1]);
			//can be removed for performance improvement YES
			vertexCoordinates.push_back(vertex[2]);
		}
	importer.FreeScene();
}
Mesh::MeshProcessor::MeshProcessor(){}
Mesh::MeshProcessor::~MeshProcessor(){}

void Mesh::MeshProcessor::sendToGpu(){
	glGenVertexArrays(1, &vertexArray);
	glBindVertexArray(vertexArray);

	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numFaces*3*sizeof(uint32_t), faceData.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, numVertices*3*sizeof(float), vertexPositions.data(), GL_STATIC_DRAW);

	if(this->hasNormals){
		glGenBuffers(1, &vertexNormalBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexNormalBuffer);
		glBufferData(GL_ARRAY_BUFFER, numVertices*3*sizeof(float), vertexNormals.data(), GL_STATIC_DRAW);
	}

	if(isTextured){
		glGenBuffers(1, &vertexTexturecoordID);
		glBindBuffer(GL_ARRAY_BUFFER, vertexTexturecoordID);
		//should only be vec2, but are vec3
		glBufferData(GL_ARRAY_BUFFER, numVertices*3*sizeof(float), vertexCoordinates.data(), GL_STATIC_DRAW);
	}

}

Mesh Mesh::loadArchivedMesh(std::string _archive){
	std::ifstream ifs(_archive, std::fstream::in | std::fstream::binary);
	boost::archive::binary_iarchive ia(ifs);
	
	MeshProcessor a;
	ia >> a;

	Mesh *p=(Mesh*)&(a.hasNormals);
	Mesh b=*p;

	return b;
}

Mesh::Mesh(){}
Mesh::Mesh(const Mesh & _mesh){
	hasNormals=_mesh.hasNormals;
	isTextured=_mesh.isTextured;
	numFaces=_mesh.numFaces;
	numVertices=_mesh.numVertices;
	vertexArray=_mesh.vertexArray;
	vertexBuffer=_mesh.vertexBuffer;
	indexBuffer=_mesh.indexBuffer;
	vertexNormalBuffer=_mesh.vertexNormalBuffer;
	vertexTexturecoordID=_mesh.vertexTexturecoordID;
}
Mesh *Mesh::getMesh(std::string _name){
	if(meshTable.find(_name)==meshTable.end()){
		if(_name[_name.length()-4]=='.')
			meshTable[_name]=new Mesh(loadArchivedMesh(_name));
		else
			meshTable[_name]=new Mesh(loadArchivedMesh(directory+_name+filetype));
	}

	return meshTable[_name];
}
void Mesh::archiveMesh(std::string _meshstring, std::string _archive){
	if(_meshstring[_meshstring.length()-4]!='.')
		_meshstring="./objects/"+_meshstring+".obj";
		
	MeshProcessor proc(_meshstring);

	if(_archive==""){
		_meshstring.replace(_meshstring.begin(), _meshstring.begin()+_meshstring.find_last_of("/")+1, "");
		_meshstring.replace(_meshstring.begin()+_meshstring.find_last_of("."), _meshstring.end(), filetype);
		_archive=directory+_meshstring;
	}
	std::ofstream ofs(_archive, std::fstream::out | std::fstream::binary);
	boost::archive::binary_oarchive oa(ofs);
	oa << proc;
}

Mesh::~Mesh(){
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &indexBuffer);
	glDeleteVertexArrays(1, &vertexArray);
}

void Mesh::draw(){
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	if(this->hasNormals){
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, vertexNormalBuffer);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}
	
	if(this->isTextured){
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, vertexTexturecoordID);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glDrawElements(GL_TRIANGLES, numFaces*3, GL_UNSIGNED_INT, 0);

	glDisableVertexAttribArray(0);
	
	if(hasNormals)
		glDisableVertexAttribArray(1);
	
	if(isTextured)
		glDisableVertexAttribArray(2);
}
std::unordered_map<std::string, Mesh*> Mesh::meshTable;
Assimp::Importer Mesh::MeshProcessor::importer;
std::string Mesh::directory("./archives/meshes/");
std::string Mesh::filetype(".arc");

