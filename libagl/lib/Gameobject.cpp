template<class Archive>
void Gameobject::serialize(Archive & ar, const unsigned int file_version){
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Gameobject::save(Archive & ar, const unsigned int version) const{
	ar << type;
	ar << name;
	ar << transform;
	ar << enabled;
	ar << mc.name;
	ar << drawable;
	ar << maco.name;
	Mesh::archiveMesh(mc.name);
}
template<class Archive>
void Gameobject::load(Archive & ar, const unsigned int version){
	ar >> type;
	ar >> name;
	ar >> transform;
	ar >> enabled;
	ar >> mc.name;
	ar >> drawable;
	ar >> maco.name;
	mc.mesh=Mesh::getMesh(mc.name);
	calcModelMatrix();
	maco.material=defaultMaterial;
}
	
void Gameobject::calcModelMatrix(){
	if((mc.mesh==NULL)||(maco.material==NULL))
		;//return;

	auto id=mat4();
	
	modelMatrix=glm::translate(id, getPosition());
	
	auto _rotation=getRotation();
	modelMatrix*=glm::rotate(id, _rotation.x, vec3(1.f, .0f, .0f))*glm::rotate(id, _rotation.y, vec3(.0f, 1.f, .0f))*glm::rotate(id, _rotation.z, vec3(.0f, .0f, 1.f));
	
	modelMatrix*=glm::scale(id, getScale());
}

Gameobject::Gameobject():enabled(true),script(NULL),transform(vec3(.0f)){
	type=0;
	setId();
}
Gameobject::Gameobject(std::string _name):name(_name), enabled(true),script(NULL),transform(vec3(.0f)){
	type=0;
	setId();
	calcModelMatrix();
	maco.material=defaultMaterial;
}
Gameobject::Gameobject(const Gameobject & _gameobject):transform(vec3(.0f)), name(_gameobject.name), enabled(_gameobject.enabled), drawable(_gameobject.drawable), script(NULL){
	type=_gameobject.type;
	mc.name = _gameobject.mc.name;
	maco.name = _gameobject.maco.name;
	maco.material=_gameobject.maco.material;
	mc.mesh=_gameobject.mc.mesh;
	setTransform(_gameobject.getTransform());
	id=_gameobject.id;
}
Gameobject::~Gameobject(){
	safeDelete(script);
	for(auto _child:children)
		delete _child;
	//delete this->mc.mesh;
}

void Gameobject::setId(){
	id=nextid++;
}
uint64_t Gameobject::getId() const{
	return id;
}
void Gameobject::draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds){
	if(!isEnabled())
		return;
		
	if(drawable){
		mvp = _camera->getProjection() * _camera->getView() * modelMatrix;
	
		glUseProgram(maco.material->program);
	
		maco.material->attributes[0].submit(mvp);
		maco.material->attributes[1].submit(modelMatrix);
		auto a=glGetUniformLocation(maco.material->program, "pointer");
		uint64_t lp=(uint64_t)this;
		float adr=(float)lp;
		glUniform1f(a, adr);
		
		mc.mesh->draw();
	}
	
	if(script!=NULL)
		scripteds->push_back(this);
		
	for(auto _child:children)
		_child->draw(_camera, ambientlights, pointlights, scripteds);

}
void Gameobject::draw(Camera * _camera){}

bool Gameobject::isEnabled() const{
	return enabled;
}
void Gameobject::setEnabled(bool _enabled){
	enabled=_enabled;
}
void Gameobject::enableDrawing(){
	drawable=true;
	calcModelMatrix();
}
void Gameobject::disbleDrawing(){
	drawable=false;
}
void Gameobject::setScript(const std::string & file){
	script=new Script<Gameobject>(file, this);
}
Script<Gameobject> *Gameobject::getScript() const{
	return script;
}
void Gameobject::setMesh(std::string _mesh){
	mc.name=_mesh;
	mc.mesh=Mesh::getMesh(_mesh);
	setEnabled(true);
	enableDrawing();
}
std::string Gameobject::getName() const{
	return name;
}
void Gameobject::setName(const std::string & _name){
	name=_name;
}
Transform Gameobject::getTransform() const{
	return transform;
}
vec3 Gameobject::getPosition() const{
	return transform.position;
}
vec3 Gameobject::getRotation() const{
	return transform.rotation;
}
vec3 Gameobject::getScale() const{
	return transform.scale;
}
void Gameobject::setTransform(const Transform & _transform){
	transform=_transform;
	calcModelMatrix();
}
void Gameobject::setPosition(const vec3 & _position){
	transform.position=_position;
	calcModelMatrix();
}
void Gameobject::setRotation(const vec3 & _rotation){
	transform.rotation=_rotation;
	calcModelMatrix();
}
void Gameobject::setScale(const vec3 & _scale){
	transform.scale=_scale;
	calcModelMatrix();
}
void Gameobject::addCamera(float _fovy, float _aspectRatio, float _nearClip, float _farClip){
	auto _child=new Camera(_fovy, _aspectRatio, _nearClip, _farClip);
	children.push_back(_child);
}
void Gameobject::addGameobject(std::string _name){
	auto _child=new Gameobject(_name);
	children.push_back(_child);
}
void Gameobject::addAmbientlight(vec4 color){
	auto _child=new Ambientlight(color);
	children.push_back(_child);
}
void Gameobject::addPointlight(vec3 color, vec3 position, vec3 attenuation){
	auto _child=new Pointlight(.2f, color, attenuation, position);
	children.push_back(_child);
}
bool Gameobject::removeChild(uint64_t id){
	for(auto _child=children.begin();_child!=children.end();_child++){
		auto child=*_child;
		if(child->getId()==id){
			delete child;
			children.erase(_child);
			return true;
		}
		if(child->removeChild(id))
			return true;
	}
		
	return false;
}
bool Gameobject::removeChild(const std::string & _name){
	for(auto _child=children.begin();_child!=children.end();_child++){
		auto child=*_child;
		if(child->getName()==_name){
			delete child;
			children.erase(_child);
			return true;
		}
		if(child->removeChild(_name))
			return true;
	}
		
	return false;
}

Gameobject *Gameobject::find(uint64_t id){
	if(getId()==id)
		return this;
	for(auto _child:children){
		Gameobject *result=_child->find(id);
		if(result!=NULL)
			return _child;
	}
	return NULL;
}
Gameobject *Gameobject::find(const std::string & _name){
	if(getName()==_name)
		return this;
	for(auto _child:children){
		Gameobject *result=_child->find(_name);
		if(result!=NULL)
			return _child;
	}
	return NULL;
}
Gameobject *Gameobject::getLastChild(){
	return children.back();
}
Gameobject *Gameobject::spawn(const Transform & _transform){
	auto c=new Gameobject(*this);
	c->setTransform(_transform);
	children.push_back(c);
	return c;
}

uint64_t Gameobject::nextid=0;
Material *Gameobject::defaultMaterial;
