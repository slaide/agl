void log(std::string msg){
	#ifdef DEBUG
	Timer::getSystemTime().print(stdout);
	printf(" | %s\n", msg.c_str());
	fflush(stdout);
	#endif
}

void log(const char *msg){
	#ifdef DEBUG
	log(std::string(msg));
	#endif
}

void errorlog(std::string msg){
	Timer::getSystemTime().print(stderr);
	fprintf(stderr, " | %s\n", msg.c_str());
	fflush(stderr);
}

void errorlogquit(std::string msg, int error){
	errorlog(msg);
	exit(error);
}

void errorlogquit(const char *msg, int error){
	errorlogquit(std::string(msg), error);
}

void glfwErrorCallback(int error, const char *errormsg){
	std::ostringstream stringstream;
	stringstream << "glfw errorcode: " << error << ": " << errormsg << std::endl;
	std::string test = stringstream.str();
	errorlog(test);
}

//check how this can be done as template for more customization
uivec2wrapper::uivec2wrapper(void(*fct)(void*, vec2)):fptr(fct){}
void uivec2wrapper::operator()(void* me, vec2 pos){
	(*this->fptr)(me, pos);
}
