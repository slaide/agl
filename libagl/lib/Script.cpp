class MyKey{
	private:
		int key;
		int getKeyState() const{
			uint32_t n=0;
			return app->getWindow(n)->getsimplekeystate(key);
		}
	public:

		static int *keysarray;
		static MyKey *keys;
		
		MyKey(int _key):key(_key){}
		~MyKey(){}
		
		bool isDown() const{
			return getKeyState()==down;
		}
		bool isUp() const{
			return getKeyState()==up;
		}
		bool isPressed() const{
			return getKeyState()==pressed;
		}
		bool isUnpressed() const{
			return getKeyState()==unpressed;
		}
};
MyKey *MyKey::keys=new MyKey[40]{
	MyKey(Key::A),
	MyKey(Key::B),
	MyKey(Key::C),
	MyKey(Key::D),
	MyKey(Key::E),
	MyKey(Key::F),
	MyKey(Key::G),
	MyKey(Key::H),
	MyKey(Key::I),
	MyKey(Key::J),
	MyKey(Key::K),
	MyKey(Key::L),
	MyKey(Key::M),
	MyKey(Key::N),
	MyKey(Key::O),
	MyKey(Key::P),
	MyKey(Key::Q),
	MyKey(Key::R),
	MyKey(Key::S),
	MyKey(Key::T),
	MyKey(Key::U),
	MyKey(Key::V),
	MyKey(Key::W),
	MyKey(Key::X),
	MyKey(Key::Y),
	MyKey(Key::Z),
	MyKey(Key::NUM0),
	MyKey(Key::NUM1),
	MyKey(Key::NUM2),
	MyKey(Key::NUM3),
	MyKey(Key::NUM4),
	MyKey(Key::NUM5),
	MyKey(Key::NUM6),
	MyKey(Key::NUM7),
	MyKey(Key::NUM8),
	MyKey(Key::NUM9),
	MyKey(Key::SPACE),
	MyKey(Key::TAB),
	MyKey(Key::ENTER),
	MyKey(Key::ESC)
};

Vec2 getRelativeMousePosition(){
	uint32_t a=0;
	return Vec2(app->getWindow(a)->relativeMousePosition);
}
Vec2 getAbsoluteMousePosition(){
	uint32_t a=0;
	return Vec2(app->getWindow(a)->absoluteMousePosition);
}
void setMouse(bool enabled){
	uint32_t a=0;
	app->getWindow(a)->setMouse(enabled);
}

template <class C>
void Script<C>::stdstuff(C *me){
	state=luaL_newstate();
	luaL_openlibs(state);

	luabridge::getGlobalNamespace(state)
		.beginNamespace("agl")
			.beginClass<MyKey>("Key")
				.addConstructor<void(*)(int)>()
				.addProperty("pressed", &MyKey::isPressed)
				.addProperty("unpressed", &MyKey::isUnpressed)
				.addProperty("up", &MyKey::isUp)
				.addProperty("down", &MyKey::isDown)
				.addStaticData("A", &MyKey::keys[0])
				.addStaticData("B", &MyKey::keys[1])
				.addStaticData("C", &MyKey::keys[2])
				.addStaticData("D", &MyKey::keys[3])
				.addStaticData("E", &MyKey::keys[4])
				.addStaticData("F", &MyKey::keys[5])
				.addStaticData("G", &MyKey::keys[6])
				.addStaticData("H", &MyKey::keys[7])
				.addStaticData("I", &MyKey::keys[8])
				.addStaticData("J", &MyKey::keys[9])
				.addStaticData("K", &MyKey::keys[10])
				.addStaticData("L", &MyKey::keys[11])
				.addStaticData("M", &MyKey::keys[12])
				.addStaticData("N", &MyKey::keys[13])
				.addStaticData("O", &MyKey::keys[14])
				.addStaticData("P", &MyKey::keys[15])
				.addStaticData("Q", &MyKey::keys[16])
				.addStaticData("R", &MyKey::keys[17])
				.addStaticData("S", &MyKey::keys[18])
				.addStaticData("T", &MyKey::keys[19])
				.addStaticData("U", &MyKey::keys[20])
				.addStaticData("V", &MyKey::keys[21])
				.addStaticData("W", &MyKey::keys[22])
				.addStaticData("X", &MyKey::keys[23])
				.addStaticData("Y", &MyKey::keys[24])
				.addStaticData("Z", &MyKey::keys[25])
				.addStaticData("NUM0", &MyKey::keys[26])
				.addStaticData("NUM1", &MyKey::keys[27])
				.addStaticData("NUM2", &MyKey::keys[28])
				.addStaticData("NUM3", &MyKey::keys[29])
				.addStaticData("NUM4", &MyKey::keys[30])
				.addStaticData("NUM5", &MyKey::keys[31])
				.addStaticData("NUM6", &MyKey::keys[32])
				.addStaticData("NUM7", &MyKey::keys[33])
				.addStaticData("NUM8", &MyKey::keys[34])
				.addStaticData("NUM9", &MyKey::keys[35])
				.addStaticData("SPACE", &MyKey::keys[36])
				.addStaticData("TAB", &MyKey::keys[37])
				.addStaticData("ENTER", &MyKey::keys[38])
				.addStaticData("ESCAPE", &MyKey::keys[39]) 
			.endClass()
			.beginClass<Vec3>("Vec3")
				.addConstructor<void(*)(float, float, float)>()
				.addProperty("x", &Vec3::getX, &Vec3::setX)
				.addProperty("y", &Vec3::getY, &Vec3::setY)
				.addProperty("z", &Vec3::getZ, &Vec3::setZ)
				.addFunction("rotateX", &Vec3::rotateX)
				.addFunction("rotateY", &Vec3::rotateY)
				.addFunction("rotateZ", &Vec3::rotateZ)
				.addFunction("add", &Vec3::operator+)
				.addFunction("sub", &Vec3::operator-)
				.addFunction("mul", &Vec3::operator*)
				.addFunction("div", &Vec3::operator/)
				.addProperty("length", &Vec3::length)
				.addFunction("normalize", &Vec3::normalize)
			.endClass()
			.beginClass<Transform>("Transform")
				.addConstructor<void(*)(Vec3, Vec3, Vec3)>()
				.addProperty("position", &Transform::getPosition, &Transform::setPosition)
				.addProperty("rotation", &Transform::getRotation, &Transform::setRotation)
				.addProperty("scale", &Transform::getScale, &Transform::setScale)
			.endClass()
			.beginClass<Vec2>("Vec2")
				.addProperty("x", &Vec2::getX, &Vec2::setX)
				.addProperty("y", &Vec2::getY, &Vec2::setY)
				.addFunction("add", &Vec2::operator+)
				.addFunction("sub", &Vec2::operator-)
				.addFunction("mul", &Vec2::operator*)
				.addFunction("div", &Vec2::operator/)
			.endClass()
			.beginClass<Gameobject>("Gameobject")
				.addData("name", &Gameobject::name)
				.addProperty("transform", &Gameobject::getTransform, &Gameobject::setTransform)
				.addFunction("spawn", &Gameobject::spawn)
			.endClass()
			.deriveClass<Camera, Gameobject>("Camera")
				.addConstructor<void(*)(void)>()
				.addProperty("target", &Camera::getTargetLua)
			.endClass()
			.addFunction("relMousepos", &getRelativeMousePosition)
			.addFunction("absMousepos", &getAbsoluteMousePosition)
			.addFunction("setMouse", &setMouse)
		.endNamespace();
	
	luabridge::push(state, me);
	lua_setglobal(state, "gameobject");
}

template <class C>
Script<C>::Script(std::string _file, C *me){
	stdstuff(me);

	std::string totalpath=directory+_file;
	luaL_dofile(state, totalpath.c_str());
	
	funcs=new luabridge::LuaRef[2]{
		{luabridge::getGlobal(state, "update")}, 
		{luabridge::getGlobal(state, "fixedUpdate")}
	};
}
template <>
Script<Ui>::Script(std::string _file, Ui *me){
	stdstuff(me);
	
	std::string totalpath=directory+_file;
	luaL_dofile(state, totalpath.c_str());
	
	funcs=new luabridge::LuaRef[3]{
		{luabridge::getGlobal(state, "mouseEnter")},
		{luabridge::getGlobal(state, "mouseOver")},
		{luabridge::getGlobal(state, "mouseLeave")}
	};
}
template <class C>
Script<C>::~Script(){
	delete funcs;
	lua_close(state);
}

template <class C>
int Script<C>::update(){
	try{
		funcs[0]();
	}catch(luabridge::LuaException e){
		return -1;
	}
	return 0;
}
template <class C>
int Script<C>::fixedUpdate(){
	try{
		funcs[1]();
	}catch(luabridge::LuaException e){
		return -1;
	}
			return 0;
}
template <class C>
std::string Script<C>::directory("./scripts/");
