GLboolean comparegreater(vec2 a, vec2 b){
	return (a.x>b.x)*(a.y>b.y);
}
GLboolean comparesmaller(vec2 a, vec2 b){
	return (a.x<b.x)*(a.y<b.y);
}
isin(vec2 point, vec2 lowerleft, vec2 upperright){
	return comparegreater(point, lowerleft)&&comparesmaller(point, upperright);
}
