template<class Archive>
void Transform::serialize(Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Transform::save(Archive & ar, const unsigned int version) const
{
	ar << position.x << position.y << position.z;
	ar << rotation.x << rotation.y << rotation.z;
	ar << scale.x    << scale.y    << scale.z;
}
template<class Archive>
void Transform::load(Archive & ar, const unsigned int version)
{
	ar >> position.x >> position.y >> position.z;
	ar >> rotation.x >> rotation.y >> rotation.z;
	ar >> scale.x >> scale.y >> scale.z;
}

Transform::Transform(const vec3 _position, const vec3 _rotation, const vec3 _scale):position(_position), rotation(_rotation), scale(_scale){}
Transform::Transform(const Vec3 _position, const Vec3 _rotation, const Vec3 _scale):position(_position.vec), rotation(_rotation.vec), scale(_scale.vec){}
Transform::~Transform(){}

Vec3 Transform::getPosition() const{return Vec3(position);}
Vec3 Transform::getRotation() const{return Vec3(rotation);}
Vec3 Transform::getScale() const{return Vec3(scale);}
void Transform::setPosition(Vec3 _v){position=_v.vec;}
void Transform::setRotation(Vec3 _v){rotation=_v.vec;}
void Transform::setScale(Vec3 _v){scale=_v.vec;}
