Application::Application(const char *name){
	title=name;
}
Application::~Application(){}
void Application::run(){}
void Application::update(){}
std::string Application::getName(){
	return title;
}

Application2D::Application2D(const char *name, std::vector<int[4]> windowInfo):Application(name){
	init2D();
	
	for(auto info:windowInfo)
		windows.emplace_back(name, info);
}
Application2D::Application2D(const char *name, int windowInfo[4]): Application(name){
	init2D();

	windows.emplace_back(name, windowInfo);
}
Application2D::~Application2D(){
	glfwTerminate();
}
		
void Application2D::closeWindow(int pos){
	windows[pos].close();
}
void Application2D::run(){
	
	while(windows.size()!=0){
		glfwPollEvents();
		update();
		
		for(auto window=windows.begin();window!=windows.end();window++){
			if(!window->closed()){
				window->draw();
				window->updateInput();
			}else{
				windows.erase(window);
				window--;
			}
		}
	}
}
void Application2D::update(){}
Window2D *Application2D::getWindow(uint32_t a){
	windows[a].makeActive();
	return &windows[a];
}
Window2D *Application2D::getWindow(GLFWwindow *_window){
	for(auto window=windows.begin();window!=windows.end();window++)
		if(window->window==_window)
			return (Window2D *)(&window->window);
}

Application3D::Application3D(const char *name, int windowInfo[4]): Application(name){
	init3D();
	
	windows.emplace_back(name, windowInfo);
}
Application3D::~Application3D(){}

void Application3D::closeWindow(int pos){
	windows[pos].close();
}
void Application3D::run(){

	while(windows.size()!=0){
		glfwPollEvents();
		update();
		
		for(auto window=windows.begin();window!=windows.end();window++){
			if(!window->closed()){
				window->draw();
				window->updateInput();
			}else{
				windows.erase(window);
				window--;
			}
		}
	}
}
void Application3D::update(){}
void Application3D::loadScene(std::string _file, uint32_t _windowIndex){
	windows[_windowIndex].loadScene(_file);
}
Window3D *Application3D::getWindow(uint32_t _a){
	windows[_a].makeActive();
	return &windows[_a];
}
Window3D *Application3D::getWindow(GLFWwindow *_window){
	for(auto window=windows.begin();window!=windows.end();window++)
		if(window->window==_window)
			return (Window3D *)(&window->window);
}
void Application3D::close(){
	for(auto & window:windows)
		window.close();
}
