template<class Archive>
void Pointlight::serialize(Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Pointlight::save(Archive & ar, const unsigned int version) const
{
	ar << boost::serialization::base_object<const Light>(*this);
	ar << constant << linear << exponent << diffuse << specularPower << specular;
}
template<class Archive>
void Pointlight::load(Archive & ar, const unsigned int version)
{
	ar >> boost::serialization::base_object<Light>(*this);
	ar >> constant >> linear >> exponent >> diffuse >> specularPower >> specular;
	calcScale();
}

void Pointlight::prepareDraw(vec2 _screensize, Camera *_camera){
	//enable reading from, disable writing to depth buffer
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	//glDisable(GL_DEPTH_TEST);
	//pointlights only need to be drawn where their backfaces are occluded by gbuffer geometry
	glDepthFunc(GL_GREATER);
	//enable culling because sphere has two sides therefore light would be drawn twice
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glUseProgram(Pointlight::pointlightProgram->program);
	
	Pointlight::pointlightProgram->attributes[0].submit(gbufferTextures[0]);
	Pointlight::pointlightProgram->attributes[1].submit(gbufferTextures[1]);
	Pointlight::pointlightProgram->attributes[2].submit(gbufferTextures[2]);
	
	Pointlight::pointlightProgram->attributes[5].submit(_screensize);
	
	Pointlight::pointlightProgram->attributes[12].submit(_camera->getPosition());
}

Pointlight::Pointlight(){
	type=2;
}
Pointlight::Pointlight(GLfloat _diffuse, vec3 & _color, vec3 & _attenuation, vec3 _newposition):Light(_color){
	type=2;
	diffuse=_diffuse;
	constant=_attenuation.x;
	linear=_attenuation.y;
	exponent=_attenuation.z;
	setPosition(_newposition);
	
	specular=1.f;
	specularPower=5.f;
	
	calcScale();
}
Pointlight::~Pointlight(){}

/*void Pointlight::calcModelMatrix(){
	modelMatrix=mat4();
	
	modelMatrix=glm::scale(modelMatrix, getScale());
	
	modelMatrix=glm::translate(modelMatrix, getPosition());
}*/
void Pointlight::calcScale(){
	GLfloat maximum=std::max(std::max(color.x, color.y), color.z);
	setScale(vec3(
		2.f*(-linear+std::sqrt
			(linear*linear-4.f*exponent*
				(constant-(256.0f/5.f)*maximum)))
		/(2.f*exponent)));
	calcModelMatrix();
}
void Pointlight::draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds){
	Pointlight *_pl=(Pointlight*)this;
	pointlights->push_back(_pl);
	
	if(script!=NULL)
		scripteds->push_back(this);
		
	for(auto _child:children)
		_child->draw(_camera, ambientlights, pointlights, scripteds);
}
void Pointlight::draw(Camera * _camera){

	mat4 mvp = _camera->getProjection() * _camera->getView() * modelMatrix;
	
	pointlightProgram->attributes[3].submit(mvp);
	pointlightProgram->attributes[4].submit(modelMatrix);
	
	pointlightProgram->attributes[6].submit(color);
	pointlightProgram->attributes[7].submit(getPosition());
	pointlightProgram->attributes[8].submit(constant);
	pointlightProgram->attributes[9].submit(linear);
	pointlightProgram->attributes[10].submit(exponent);
	pointlightProgram->attributes[11].submit(specular);
	
	mesh->draw();
}
Material *Pointlight::pointlightProgram;
Mesh *Pointlight::mesh;
