template<class Archive>
void Scene::serialize(Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Scene::save(Archive & ar, const unsigned int version) const
{
	ar << name;
	ar << chunks;
}
template<class Archive>
void Scene::load(Archive & ar, const unsigned int version)
{
	ar >> name;
	ar >> chunks;
}
Scene::Scene(){}
Scene::Scene(std::string & _name, vec2 _screenSize):name(_name), canvas(vec4(1.f, 1.f, 1.f, 0.f), _screenSize, vec2(0.f)){}
Scene::~Scene(){
	for(auto _c:chunks)
		delete(_c);
}

void Scene::loadChunk(std::string _archive){
	std::ifstream ifs(_archive, std::fstream::in | std::fstream::binary);
	boost::archive::binary_iarchive ia(ifs);

	Chunk *a;
	ia >> a;
	chunks.push_back(a);
}
void Scene::loadChunk(int num){
	std::string _archive=std::string("archives/chunks/chunk")+std::to_string(num)+filetype;
	
	std::ifstream ifs(_archive, std::fstream::in | std::fstream::binary);
	boost::archive::binary_iarchive ia(ifs);

	Chunk *a;
	ia >> a;
	chunks.push_back(a);
}
void Scene::saveChunks(){
	for(int num=0;num<chunks.size();num++){
		std::string _archive=std::string("archives/chunks/chunk")+std::to_string(num)+filetype;

		std::ofstream ofs(_archive, std::fstream::out | std::fstream::binary);
		boost::archive::binary_oarchive oa(ofs);
		oa << chunks[num];
	}
}

std::string Scene::getName(){
	return name;
}
void Scene::setName(std::string _name){
	name=_name;
}
void Scene::bake(std::string archive){
	//write scene to file
	if(archive==""){
		archive=directory+name+filetype;
	}
	std::ofstream ofs(archive, std::fstream::out | std::fstream::binary);
	boost::archive::binary_oarchive oa(ofs);
	oa << *this;
	
	//archive static meshes for lights
	Mesh::archiveMesh("./objects/sphere.obj");
	Mesh::archiveMesh("./objects/box.obj");
	Mesh::archiveMesh("./objects/screen.obj");
}
Scene Scene::load(std::string archive){
	if(archive[archive.length()-4]!='.')
		archive=directory+archive+filetype;
	std::ifstream ifs(archive, std::fstream::in | std::fstream::binary);
	boost::archive::binary_iarchive ia(ifs);
	Scene a;
	ia >> a;
	
	return a;
}
void Scene::drawCanvas(vec2 _screensize){
	canvas.drawAll(_screensize);
}
void Scene::drawGeometry(){
	for(auto _chunk:chunks)
		_chunk->draw(mainCamera, &ambientlights, &pointlights, &scripteds);
}
void Scene::drawAmbientlights(vec2 _screen){
	Ambientlight::prepareDraw(_screen);
	
	for(auto ambientlight:ambientlights)
		ambientlight->draw(NULL);
}
void Scene::drawPointlights(vec2 _screen){
	Pointlight::prepareDraw(_screen, mainCamera);
	
	for(auto _pl:pointlights)
		_pl->draw(mainCamera);
}

void Scene::drawLightsAndUi(vec2 _screensize){
	drawAmbientlights(_screensize);
	drawPointlights(_screensize);
	canvas.drawAll(_screensize);
	
	for(auto obj:scripteds){
		auto script=obj->getScript();
		script->update();
	}
	
	//step physics
	
	for(auto obj:scripteds){
		auto script=obj->getScript();
		script->fixedUpdate();
	}
	
	ambientlights.clear();
	pointlights.clear();
	scripteds.clear();
}
		
bool Scene::removeGameobject(uint64_t id){
	for(auto _chunk:chunks){
		if(_chunk->removeChild(id))
			return true;
	}
	return false;
}
bool Scene::removeGameobject(const std::string & _name){
	for(auto _chunk:chunks){
		if(_chunk->removeChild(_name))
			return true;
	}
	return false;
}
Gameobject *Scene::find(uint64_t id){
	for(auto _chunk:chunks){
		Gameobject *result=_chunk->find(id);
		if(result!=NULL)
			return _chunk;
	}
	return NULL;
}
Gameobject *Scene::find(const std::string & _name){
	for(auto _chunk:chunks){
		Gameobject *result=_chunk->find(_name);
		if(result!=NULL)
			return _chunk;
	}
	return NULL;
}

std::string Scene::directory("./archives/scenes/");
std::string Scene::filetype(".arc");
