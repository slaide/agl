Material::Attribute::Attribute(const char *name, GLint program){
	this->location=glGetUniformLocation(program, name);
}
Material::Attribute::~Attribute(){}

inline void Material::Attribute::submit(int32_t value){
	glUniform1i(this->location, value);
}
inline void Material::Attribute::submit(uint32_t value){
	glUniform1ui(this->location, value);
}
inline void Material::Attribute::submit(float value){
	glUniform1f(this->location, value);
}
inline void Material::Attribute::submit(float *value){
	glUniform1f(this->location, *value);
}
inline void Material::Attribute::submit(vec2 value){
	glUniform2f(this->location, value.x, value.y);
}
inline void Material::Attribute::submit(vec3 value){
	glUniform3f(this->location, value.x, value.y, value.z);
}
inline void Material::Attribute::submit(vec4 value){
	glUniform4f(this->location, value.x, value.y, value.z, value.w);
}
inline void Material::Attribute::submit(mat4 value){
	glUniformMatrix4fv(this->location, 1, GL_FALSE, &value[0][0]);
}

Material::Material(){}
Material::Material(const char *vertexShader, const char *fragmentShader){
	uint32_t vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	uint32_t fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertexShaderCode;
	std::ifstream vertexShaderStream(vertexShader, std::ios::in);
	if(vertexShaderStream.is_open()){
		std::string Line = "";
		while(getline(vertexShaderStream, Line))
			vertexShaderCode += "\n" + Line;
		vertexShaderStream.close();
	}else{
		errorlog("could not read vertexshader: ");
		errorlog(vertexShader);
	}

	std::string fragmentShaderCode;
	std::ifstream fragmentShaderStream(fragmentShader, std::ios::in);
	if(fragmentShaderStream.is_open()){
		std::string Line = "";
		while(getline(fragmentShaderStream, Line))
			fragmentShaderCode += "\n" + Line;
		fragmentShaderStream.close();
	}else{
		errorlog("could not read fragmentshader: ");
		errorlog(fragmentShader);
	}

	int32_t Result = GL_FALSE;
	int InfoLogLength;

	char const * VertexSourcePointer = vertexShaderCode.c_str();
	glShaderSource(vertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(vertexShaderID);

	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 1 ){
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(vertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s %s %s %s\n", "vertexshader problem (", vertexShader, "): ", &VertexShaderErrorMessage[0]);
	}

	char const * FragmentSourcePointer = fragmentShaderCode.c_str();
	glShaderSource(fragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(fragmentShaderID);

	glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 1 ){
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(fragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s %s %s %s\n", "fragmentshader problem (", fragmentShader, "): ", &FragmentShaderErrorMessage[0]);
	}

	uint32_t program = glCreateProgram();
	glAttachShader(program, vertexShaderID);
	glAttachShader(program, fragmentShaderID);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &Result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 1 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(program, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s %s\n", "shaderprogram problem: ", &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	glDetachShader(program, vertexShaderID);
	glDetachShader(program, fragmentShaderID);

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	this->program=program;
}
Material::~Material(){
	glDeleteProgram(this->program);
}

int Material::addAttribute(const char *name){
	this->attributes.emplace(this->attributes.end(), name, this->program);
}
Material::Attribute *Material::Attribute::shit;
