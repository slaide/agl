template<class Archive>
void Ambientlight::serialize( Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Ambientlight::save(Archive & ar, const unsigned int version) const
{
	ar << boost::serialization::base_object<const Light>(*this);
	ar << ambient;
}
template<class Archive>
void Ambientlight::load(Archive & ar, const unsigned int version)
{
	ar >> boost::serialization::base_object<Light>(*this);
	ar >> ambient;
}

//void Ambientlight::calcModelMatrix(){}

void Ambientlight::prepareDraw(vec2 _screensize){
	glDisable(GL_DEPTH_TEST);
	
	glUseProgram(ambientlightProgram->program);
	
	ambientlightProgram->attributes[0].submit(gbufferTextures[0]);
	ambientlightProgram->attributes[1].submit(gbufferTextures[1]);
	ambientlightProgram->attributes[2].submit(gbufferTextures[2]);
	
	ambientlightProgram->attributes[5].submit(_screensize);
}

Ambientlight::Ambientlight():Light(v1), ambient(1.f){
	type=1;
}
Ambientlight::Ambientlight(vec4 & color):Light(color), ambient(color.w){
	type=1;
}
Ambientlight::~Ambientlight(){}

void Ambientlight::draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds){
	Ambientlight *_al=(Ambientlight*)this;
	ambientlights->push_back(_al);
	
	if(script!=NULL)
		scripteds->push_back(this);
		
	for(auto _child:children)
		_child->draw(_camera, ambientlights, pointlights, scripteds);
}
void Ambientlight::draw(Camera * _camera){
	ambientlightProgram->attributes[3].submit(this->color);
	ambientlightProgram->attributes[4].submit(this->ambient);
	
	this->mesh->draw();
}
Material *Ambientlight::ambientlightProgram;
Mesh *Ambientlight::mesh;
