GBuffer::GBuffer(vec2 _size, uint32_t _numTextures){
	if(_numTextures==0)
		return;
	size=_size;
	numTextures=_numTextures;
	//setup gbuffer
	glGenFramebuffers(1, &customFramebuffer);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, customFramebuffer);

	glGenTextures(numTextures-1, textures);
	
	int _a=numTextures-2; //-2
	for(GLuint i=0;i<_a+1;i++){
		glBindTexture(GL_TEXTURE_2D, textures[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, size.x, size.y, 0, GL_RGB, GL_FLOAT, NULL);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, textures[i],0);
		drawBuffers[i]=GL_COLOR_ATTACHMENT0+i;
	}
	
	int _b=_a+1;
	glGenRenderbuffers(1, &textures[_b]);
	glBindRenderbuffer(GL_RENDERBUFFER, textures[_b]);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size.x, size.y);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, textures[_b]);
	
	glDrawBuffers(_a+2, drawBuffers);

	GLenum status=glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status!=GL_FRAMEBUFFER_COMPLETE){
		//printf("Framebuffer error, status: 0x%x\n", status);
		if(status==GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
			log("framebuffer status: incomplete attachment");
		if(status==GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT)
			log("framebuffer status: incomplete / missing attachment");
		if(status==GL_FRAMEBUFFER_UNSUPPORTED)
			log("framebuffer unsupported");
		exit(-7);
	}

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}
GBuffer::~GBuffer(){
	int _a=numTextures-2;
	glDeleteRenderbuffers(1, &textures[_a]);
	glDeleteTextures(_a, (const uint32_t*)&textures);
	glDeleteFramebuffers(1, &customFramebuffer);
}
void GBuffer::resize(uint32_t width, uint32_t height){
	//bullshit
	for(GLuint i=0;i<numTextures-1;i++){
		glBindTexture(GL_TEXTURE_2D, textures[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	}
	glBindRenderbuffer(GL_RENDERBUFFER, textures[numTextures-1]);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
}
void GBuffer::prepareforGPass(){
	//bind gbuffer for drawing
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, customFramebuffer);
	glDrawBuffers(numTextures, drawBuffers);
	//enable deapth red/write
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LESS);
	//disable blending from light pass
	glDisable(GL_BLEND);
	//enable backface culling for improved performance
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	//clear color and depth from previous frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
void GBuffer::prepareforLightPass(){
	//read from gbuffer
	glBindFramebuffer(GL_READ_FRAMEBUFFER, customFramebuffer);
	//draw to window framebuffer
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	//copy depthbuffer from fbo to default framebuffer
	glBlitFramebuffer(0,0,size.x,size.y,0,0,size.x,size.y,GL_DEPTH_BUFFER_BIT,GL_NEAREST);
	//enable blending for light stacking
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);
	//set gbuffer textures to read
	int _a=numTextures-1;
	for(GLuint i=0;i<_a;i++){
		glActiveTexture(GL_TEXTURE0+i);
		glBindTexture(GL_TEXTURE_2D, textures[i]);
	}
	//clear color from last frame
	glClear(GL_COLOR_BUFFER_BIT);
}
Gameobject *GBuffer::getObject(ivec2 pos){
	float v;
	glReadBuffer(GL_COLOR_ATTACHMENT0+4);
	glReadPixels(pos.x, pos.y, 1, 1, GL_RED, GL_FLOAT, &v);
	
	uint64_t lv=(uint64_t)v;
	Gameobject *object=(Gameobject*)lv;
	return object;
}
