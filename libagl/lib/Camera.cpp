template<class Archive>
void Camera::serialize( Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Camera::save(Archive & ar, const unsigned int version) const
{
	ar << boost::serialization::base_object<const Gameobject>(*this);
	ar << fovy << aspectRatio << nearClip << farClip;
}
template<class Archive>
void Camera::load(Archive & ar, const unsigned int version)
{
	ar >> boost::serialization::base_object<Gameobject>(*this);
	ar >> fovy >> aspectRatio >> nearClip >> farClip;
	setInitial();
}

//void Camera::calcModelMatrix(){}

Camera::Camera(){}
Camera::Camera(float _fovy, float _aspectRatio, float _nearClip, float _farClip):fovy(_fovy),aspectRatio(_aspectRatio), nearClip(_nearClip),farClip(_farClip){
	setInitial();
}
Camera::~Camera(){}

void Camera::setInitial(){
	projection=glm::perspective(fovy, aspectRatio, nearClip, farClip);
	target=vec3(0.f, 0.f, -1.f);
}
vec3 Camera::getTarget() const{
	auto targ=target;
	auto _rotation=getRotation();
	targ=glm::rotateX(targ, _rotation.x);
	targ=glm::rotateY(targ, _rotation.y);
	targ=glm::rotateZ(targ, _rotation.z);
	return targ;
}
mat4 Camera::getView(){
	view=glm::lookAt(vec3(.0f),getTarget(),vec3(.0f, 1.f, .0f));
	
	view=glm::translate(view, getPosition());
	
	return view;
}
mat4 Camera::getProjection() const{
	return projection;
}
Vec3 Camera::getTargetLua() const{
	return Vec3(getTarget());
}
void Camera::setScript(const std::string & file){
	script=(Script<Gameobject>*)new Script<Camera>(file, this);
}

void Camera::draw(Camera * _camera, std::vector<Ambientlight*> *ambientlights, std::vector<Pointlight*> *pointlights, std::vector<Gameobject*> *scripteds){
	if(!isEnabled())
		return;
		
	switch(type){
		//case 0: break;
		case 1: {
				Ambientlight *al=(Ambientlight *)this;
				ambientlights->push_back(al);
			} 
			break;
		case 2: {
				Pointlight *pl=(Pointlight *)this; 
				pointlights->push_back(pl); 
			}
			break;
	}
	
	if(script!=NULL)
		scripteds->push_back(this);
		
	for(auto _child:children)
		_child->draw(_camera, ambientlights, pointlights, scripteds);
}
