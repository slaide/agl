template<class Archive>
void Light::serialize(Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Light::save(Archive & ar, const unsigned int version) const
{
	ar << boost::serialization::base_object<const Gameobject>(*this);
	ar << color.x << color.y << color.z;
}
template<class Archive>
void Light::load(Archive & ar, const unsigned int version)
{
	ar >> boost::serialization::base_object<Gameobject>(*this);
	ar >> color.x >> color.y >> color.z;
}
Light::Light(vec3 & _color):color(_color), Gameobject(""){}
Light::Light(vec4 _color):color(_color.x, _color.y, _color.z), Gameobject(""){}
Light::Light(){}
Light::~Light(){}
