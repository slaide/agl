template<class Archive>
void Canvas::serialize( Archive & ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}
template<class Archive>
void Canvas::save(Archive & ar, const unsigned int version) const
{
	ar << boost::serialization::base_object<const Ui>(*this);
}
template<class Archive>
void Canvas::load(Archive & ar, const unsigned int version)
{
	ar >> boost::serialization::base_object<Ui>(*this);
}

Canvas::Canvas(vec4 color, vec2 size, vec2 position):Ui(color, position, size, vec2(0.f)){}
Canvas::Canvas(){}
Canvas::~Canvas(){}

void Canvas::drawAll(vec2 _screensize){
	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	glUseProgram(Ui::material->program);
	Ui::material->attributes[4].submit(this->size);
	Ui::material->attributes[5].submit(this->position);
	Ui::material->attributes[6].submit(_screensize);
	
	this->draw();
}
vec2 Canvas::fhd(1920.f, 1080.f);
