#include <agl.h>

int window[4]={1280, 720, 100, 100};

class MyApp3D: public Application3D{
	public:
		MyApp3D(std::string _file):Application3D("3D App", window){
			
			auto _scene=windows[0].getBaseScene();
			
			_scene->chunks.push_back(new Chunk());
			auto base=_scene->chunks[0];
			
			base->addGameobject("bunny");
			Gameobject *_bun=base->getLastChild();
			_bun->setMesh("bunny");
			_bun->setScript("test.lua");
			
			base->addAmbientlight(vec4(1.f, 1.f, 1.f, .1f));
			
			base->addPointlight(vec3(1.f), vec3(0.0f, 0.0f, -5.0f), vec3(1.0f, 0.14f, .07f));
			//Pointlight *lit=(Pointlight *)base->getLastChild();
			
			base->addCamera();
			
			_scene->mainCamera=(Camera*)base->getLastChild();
			_scene->mainCamera->setScript("cam.lua");
		}
		~MyApp3D(){}
		
};

int main(int argc, char **argv){

	log("starting");

	app=new MyApp3D("start");
	app->run();
	
	return 0;
}
