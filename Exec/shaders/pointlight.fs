#version 330 core

uniform sampler2D colorMap;
uniform sampler2D normalMap;
uniform sampler2D positionMap;

struct Material{
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

struct Light {
    vec3 position;  
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
	
    float constant;
    float linear;
    float quadratic;
};

uniform vec2 screenSize;
uniform vec3 viewPos;
uniform Light light;

out vec4 color;

void main(){

	vec2 texCoord=gl_FragCoord.xy/screenSize;
	
	vec3 colortemp=texture(colorMap, texCoord).xyz;
	vec3 worldPos=texture(positionMap, texCoord).xyz;
	vec3 normal=texture(normalMap, texCoord).xyz;
	normal=normalize(normal);
	
    // ambient
    //vec3 ambient = light.ambient * texture(material.diffuse, TexCoords).rgb;
    vec3 ambient = light.ambient * colortemp;
    
    // diffuse 
    vec3 lightDir = -normalize(light.position - worldPos);
    float diff = max(dot(normal, lightDir), 0.0);
    //vec3 diffuse = light.diffuse * diff * texture(material.diffuse, TexCoords).rgb;  
    vec3 diffuse = light.diffuse * diff * colortemp;
    
    // specular
    vec3 viewDir = -normalize(viewPos - worldPos);
    vec3 reflectDir = reflect(-lightDir, normal);  
    //float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 0.5);
    //vec3 specular = light.specular * spec * texture(material.specular, TexCoords).rgb; 
    vec3 specular = light.specular * spec * vec3(2.0); 
    
    // attenuation
    float distance    = length(light.position - worldPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    
    ambient  *= attenuation;  
    diffuse   *= attenuation;
    specular *= attenuation;   
        
    vec3 result = ambient + diffuse + specular;
    color = vec4(result, 1.0);
	
}
