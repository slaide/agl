#version 330

uniform sampler2D colorMap;
uniform sampler2D normalMap;
uniform sampler2D positionMap;

uniform vec3 lightColor;
uniform float ambient;

uniform vec2 screenSize;

out vec4 color;

void main(){
	vec2 texCoord=gl_FragCoord.xy/screenSize;
	
	vec3 colortemp=texture(colorMap, texCoord).xyz;
	vec3 worldPos=texture(positionMap, texCoord).xyz;
	vec3 normal=texture(normalMap, texCoord).xyz;
	normal=normalize(normal);
	
	colortemp*=lightColor;
	
	color=vec4(colortemp, 1.0)*ambient;
}
