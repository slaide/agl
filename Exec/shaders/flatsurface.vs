#version 330

layout(location=0) in vec3 position;

layout(location=2) in vec3 texcoord;

uniform vec2 size;
uniform vec2 relposition;
uniform vec2 canvassize;
uniform vec2 canvasposition;
uniform vec2 screen;

out vec2 textcoord;

void main(){
	
	vec2 one=vec2(1.0);
	vec2 screenposition=position.xy+one;
	
	screenposition*=size/2;
	screenposition+=canvasposition+relposition;
	screenposition/=screen/2;

	screenposition-=one;
	
	gl_Position=vec4(screenposition, 0.0, 1.0);
	textcoord=texcoord.xy;
}
