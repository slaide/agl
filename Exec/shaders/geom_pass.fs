#version 330

layout (location=0) out vec3 diffuseOut;
layout (location=1) out vec3 normalOut;
layout (location=2) out vec3 worldPosOut;
layout (location=3) out vec3 materialValues;
layout (location=4) out vec3 objectpointer;

uniform float pointer;

in vec3 normal0;
in vec3 worldPos0;

void main(){
	diffuseOut=vec3(0.0, 1.0, 0.0);
	normalOut=normalize(normal0);
	worldPosOut=worldPos0;

	objectpointer.r=pointer;
}
