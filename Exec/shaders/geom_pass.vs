#version 330

layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;

uniform mat4 MVP;
uniform mat4 modelMatrix;

out vec3 normal0;
out vec3 worldPos0;

void main(){
	gl_Position = MVP * vec4(position, 1.0);
	normal0=(modelMatrix*vec4(normal, 0.0)).xyz;
	worldPos0=(modelMatrix*vec4(position, 1.0)).xyz;
}
