#version 330

uniform sampler2D image;
uniform vec4 bgcolor;

in vec2 textcoord;

out vec4 color;

void main(){
	if(bgcolor.x<0.0)
		{color=texture(image, textcoord.xy);}
	else
		{color=bgcolor;}
}
