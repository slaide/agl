mytrans=gameobject.transform
mypos=mytrans.position
mypos.z=-2.0
mytrans.position=mypos
gameobject.transform=mytrans
speed=0.06
rotSpeed=0.01
ui=false
function update()
	if agl.Key.Q.down then
		agl.setMouse(ui)
		if ui then
			ui=false
		else
			ui=true
		end
	end

	if ui then
		mytrans=gameobject.transform
		mypos=mytrans.position
		targ=gameobject.target:mul(speed)
	
		if agl.Key.W.pressed then
			mypos=mypos:sub(targ)
		end
		if agl.Key.S.pressed then
			mypos=mypos:add(targ)
		end
	
		targ.y=0
		targ=targ:rotateY(90.0)
	
		if agl.Key.A.pressed then
			mypos=mypos:sub(targ)
		end
		if agl.Key.D.pressed then
			mypos=mypos:add(targ)
		end
	
		myrot=mytrans.rotation
		rmp=agl.relMousepos():mul(rotSpeed)
		myrot.x=myrot.x-rmp.y
		myrot.y=myrot.y-rmp.x
		mytrans.rotation=myrot
	
		mytrans.position=mypos
		gameobject.transform=mytrans
	end
end
