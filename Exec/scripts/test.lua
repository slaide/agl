speedref=0.05
k=true
refpos=agl.Vec3(2.0, 2.0, 2.0)
count=1
function update()
	if k then
		local bunrot=agl.Vec3(0.0, 0.0, 0.0)
		local bunsc=agl.Vec3(1.0, 1.0, 1.0)
		local bunpos=refpos:mul(count)
		local buntr=agl.Transform(bunpos, bunrot, bunsc)
		local newbun=gameobject:spawn(buntr)
		count=count+1
		---[[
			k=false
		--]]
	end
end
