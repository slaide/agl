ifeq ($(OS),Windows_NT)
	compiler=g++ -mwindows -static-libgcc -static-libstdc++
	dirs=-I/mingw64/include -L/mingw64/lib -I./libagl/include -L. -I. -I/mingw64/include/bullet -I/mingw64/include/freetype2
	links=-Wl,-rpath,. -l:libagl.dll.a -lBulletDynamics -lLinearMath -lBulletCollision -lglfw3 -lassimp -lz -lboost_serialization-mt -lfreetype -lBulletDynamics -lBulletCollision -lLinearMath -llua
	exec=main.exe
else
	compiler=g++-7
	dirs= -I./libagl/include -L./libagl/
	links=-lagl
	exec=main
endif

.PHONY: Clean Debug debugrun

Debug: Engine.cpp
	$(compiler) -std=c++1z -m64 $(dirs) Engine.cpp -o ./Exec/main $(links) -O0 -static
	
Clean:
	
debugrun:
	make Debug
	./Exec/$(exec)
	
# find . -name '*.cpp' | xargs wc -l
# gcc -c dll.c -fpic
# gcc -shared -o libdll.so dll.o
# gcc -c main.c
#						dynamic for loading at runtime. leave out for loading statically (linking dynamic)
# gcc main.o -o main -rdynamic -ldll -Lpath/to/libdll.so

test:
	g++ -std=c++1z $(dirs) -c Exec/scripts/test.cpp -fPIC -o Exec/scripts/libtest.o -I./libagl/include
	g++ -std=c++1z -shared $(dirs) -o ./Exec/scripts/libtest.so Exec/scripts/libtest.o -ldl -l:libagl.dll.a -L. -L./Exec $(links)
	rm Exec/scripts/libtest.o
